<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Eelly\SDK\EellyOldCode\Goods;

use Eelly\SDK\Client as Client20191227;

class GoodsCategoryService
{
    /**
     * @param int $ifShowType
     * @return array
     * @author zhangyangxun
     * @since 2019/12/27
     */
    public static function getGoodsManageCategoryData(int $ifShowType): array
    {
        return Client20191227::requestApi('/eellyOldCode/goods/category/getGoodsManageCategoryData?tpl=1', ['ifShowType' => $ifShowType], 'array');
    }
}