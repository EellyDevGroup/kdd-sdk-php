<?php

declare (strict_types=1);

/*
 * This file is part of blty package.
 *
 * (c) blty.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace KDD\SDK\Service;

use KDD\SDK\Client;

/**
 * This class has been auto-generated by shadon compiler (2019-12-26 10:05:35).
 */
class ServiceService
{
    /**
     * 校验用户增值服务是否到期
     *
     * @param int $userId 用户id
     * @param string $toolsName 工具名
     *
     * @return array
     *
     * @author wechan
     * @since 2019年12月25日
     */
    public static function checkService(int $userId, string $toolsName) : array
    {
        return Client::requestApi('/service/internal/service/checkService?tpl=1', ['userId' => $userId, 'toolsName' => $toolsName], 'array');
    }
}