<?php

declare (strict_types=1);

/*
 * This file is part of blty package.
 *
 * (c) blty.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace KDD\SDK\System;

use KDD\SDK\Client;

/**
 * 版本控制 内部接口
 */
class AppVersionService
{
    /**
     * 获取是否有设置版本控制值
     *
     * @author wechan
     * @since 2020年3月11日
     */
    public function getAppVersionAudit()
    {
        return Client::requestApi('/system/internal/appVersion/getAppVersionAudit?tpl=1', [], 'array');
    }
}