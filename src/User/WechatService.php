<?php

declare (strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) eelly.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace KDD\SDK\User;

use KDD\SDK\Client;
use DI\Annotation\Inject;

/**
 * This class has been auto-generated by shadon compiler (2020-01-07 11:22:04).
 */
class WechatService
{
    /**
     * 获取token
     *
     * @param string $appId
     * @return string
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.1.3
     */
    public static function getAccessToken(string $appId) : string
    {
        return Client::requestApi('/user/internal/wechat/getAccessToken?tpl=1', ['appId' => $appId], 'string');
    }
    /**
     * 通过openId 获取用户信息
     *
     * @param string $appId
     * @param string $openId
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.1.3
     */
    public static function getUserInfoByOpenId(string $appId, string $openId) : array
    {
        return Client::requestApi('/user/internal/wechat/getUserInfoByOpenId?tpl=1', ['appId' => $appId, 'openId' => $openId], 'array');
    }
    /**
     * 通过openId 获取用户信息 多个
     *
     * @param string $appId
     * @param array $openIds
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.1.3
     */
    public static function getUserInfosByOpenId(string $appId, array $openIds) : array
    {
        return Client::requestApi('/user/internal/wechat/getUserInfosByOpenId?tpl=1', ['appId' => $appId, 'openIds' => $openIds], 'array');
    }
    /**
     * 微信通知信息
     *
     * @param integer $userId 用户id
     * @param string $appId appId
     * @return array
     * 
     * @author sunanzhi <sunanzhi@hotmail.com>
     * @since 2020.1.7
     */
    public static function notificationInfo(int $userId, string $appId) : array
    {
        return Client::requestApi('/user/internal/wechat/notificationInfo?tpl=1', ['userId' => $userId, 'appId' => $appId], 'array');
    }
}